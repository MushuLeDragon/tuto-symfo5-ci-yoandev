[![pipeline status](https://gitlab.com/MushuLeDragon/tuto-symfo5-ci-yoandev/badges/master/pipeline.svg)](https://gitlab.com/MushuLeDragon/tuto-symfo5-ci-yoandev/-/commits/master)
[![coverage report](https://gitlab.com/MushuLeDragon/tuto-symfo5-ci-yoandev/badges/master/coverage.svg)](https://gitlab.com/MushuLeDragon/tuto-symfo5-ci-yoandev/-/commits/master)

# Continious Integration for Symfony with Gitlab CI

## Config

Setup the containers:

```shell
cd docker
cp .env.example .env
nano .env

docker-compose up -d
```

## Init

Init the project:

```shell
symfony new gitlab --full
cd gitlab

php bin/console d:d:c
php bin/console m:e Demo
php bin/console m:mi
php bin/console d:m:m

# Create Test
php bin/console m:un

# Run Tests
php bin/phpunit
# More verbose
php bin/phpunit --testdox
```

## Tests

### Test project

```shell
php bin/console d:d:c --env=test
php bin/console d:m:m --env=test
# Create CRUD
php bin/console m:cr
php bin/console m:functional-test
```

### Other tests

```shell
docker exec -it gitlab_php bash

# Phpstan
vendor/bin/phpstan analyse src

# CodeSniffer phpcs
vendor/bin/phpcs -v --standard=PSR12 --ignore=src/Kernel.php src/ # Show errors
vendor/bin/phpcbf -v --standard=PSR12 --ignore=src/Kernel.php src/ # Show errors and fix them if possible
```

### Installation

```shell
cd gitlab

# Phpstan
composer require --dev phpstan/phpstan
composer require --dev squizlabs/PHP_CodeSniffer
```

From root project directory:

```shell
# Phpstan
vendor/bin/phpstan analyse src
# CodeSniffer phpcs
vendor/bin/phpcs -v --standard=PSR12 --ignore=src/Kernel.php src/
```

From project directory

```shell
cd gitlab

# Phpstan
vendor/bin/phpstan analyse src
# CodeSniffer phpcs
vendor/bin/phpcs -v --standard=PSR12 --ignore=src/Kernel.php src/
```